<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    // return $router->app->version();
    return \response()->json([ 'statusCode' => 200, 'Message' => 'Rest Api Dengan Lumen' ], 200);
});

/* Genereate Key Env */
$router->get('/key', function() use ($router) {
	 return \Illuminate\Support\Str::random(32);
});


/* Controller User */
$router->get('search', 'BintangController@Search');

/* Auth Controller With JWT */
$router->group(['prefix' => 'auth'], function () use ($router) {
	$router->post('login', 'AuthController@login');
	$router->post('register', 'AuthController@register');
});
