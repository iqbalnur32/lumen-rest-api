# Rest Api Lumen

# Setup
clone repo ini kedalam komputer/device anda.
```shell
git clone https://gitlab.com/iqbalnur32/lumen-rest-api.git
```

Instal library yang dibutuhkan menggunakan composer 
```shell
composer install
```

copy file `.env.example` ke `.env` lalu sesuaikan konfigurasi dengan milik anda.

Jalankan lumen dengan perintah
```shell
php -S localhost:8000 -t public
```

Menjalankan database dan seeder nya
```shell
php artisan migrate
php artisan make:seeder UserSeeder
```

Buat secret key dengan mengakses endpoint /key:
```shell
http://localhost:8000/key
```

Routing List

```shell
'/'
'/key'
'/user'
'/auth/login'
'/auth/register'
```

