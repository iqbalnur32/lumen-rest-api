<?php 

function messageForValidator() {
    return [
        'required' => ':attribute tidak boleh kosong',
        'alpha' => ':attribute hanya boleh berupa huruf',
        'unique' => ':attribute sudah dipakai',
        'exists' => ':attribute tidak terdaftar',
        'email' => ':attribute tidak valid',
        'integer' => ':attribute hanya boleh berupa bilangan bulat',
        'min' => ':attribute minimal :min karakter',
        'max' => ':attribute maximal :max karakter',
        'alpha_num' => ':attribute hanya boleh berupa huruf & angka',
        'numeric' => ':attribute hanya boleh berupa angka'
    ];
}
    
/* 
*Ini Adalah Name Validator Dari MessageForValidator  
* Example  "email": [
*               "Email sudah dipakai"
*           ]
*/
function aliasRequestNameInValidator() {
    return [
        'username'   => 'Username',
        'email'      => 'Email',
        'password'   => 'Password',
    ];
}

?>