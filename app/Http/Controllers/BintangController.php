<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Hash, Validator};
use Illuminate\Support\Str;
use App\Http\Controllers\Access\ApiResponse;
use App\Models\{User};

class BintangController extends Controller
{
	use ApiResponse;

	public function __construct(Request $request)
	{
		$this->request = $request;	
	}

	public function Search(Request $request)
	{
		$users = User::select("*")
				->where("username", "like", "%{$request->get('search')}%")
				// ->orWhere("email", "like", "%{$request->get('search')}%")
				// ->orWhere("role", "like", "%{$request->get('search')}%")
				->paginate(100);
				// ->get();
		$count_data = User::select("*")
				->where("username", "like", "%{$request->get('search')}%")
				->orWhere("email", "like", "%{$request->get('search')}%")
				->orWhere("role", "like", "%{$request->get('search')}%")
				->count();
	
		if (!$users->isEmpty()) {
			return $this->success('Success', ['Jumlah' => $count_data, 'users' => $users]);
		}

		return $this->error('Upss', ['message' => 'Maaf Pencarian Anda Tidak Ditemukan']);
	}
	
	// search?search=su&page=5 => Pagination Data query params
}

?>