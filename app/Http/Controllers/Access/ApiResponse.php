<?php

namespace App\Http\Controllers\Access;


trait ApiResponse {

    private $result = [
        "status" => false,
        "message" => "",
        "data" => [],
        "error" => []
    ];

    public function show404() {
        $this->result['status'] = false;
        $this->result['message'] = '404 Not Found';

        unset($this->result['data'], $this->result['error']);

        return \response()->json($this->result, 404);
    }

    public function success($data = [],$msg = '')
    {
        $this->result['status'] = true;
        $this->result['message'] = $msg;
        $this->result['data'] = $data;
        unset($this->result['error']);

        return \response()->json($this->result);
    }


    public function error($error = [],$msg = '', $code = 400)
    {
        $this->result['status'] = false;
        $this->result['message'] = $msg;
        $this->result['error'] = $error;

        unset($this->result['data']);

        return \response()->json($this->result,$code);
    }
}