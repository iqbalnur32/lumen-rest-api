<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Hash, Validator};
use Illuminate\Support\Str;
use App\Http\Controllers\Access\ApiResponse;
use App\Models\{User};

class AuthController extends Controller
{
	use ApiResponse;

	public function __construct()
	{
		
	}

	public function login(Request $request)
	{
		// $validate = Validator::make($request->only('username', 'password'), [
		// 	'username' => 'required',
		// 	'password' => 'required'
		// ], messageForValidator(), aliasRequestNameInValidator());

		// /* Check validate */
		// if ($validate->fails()) {
		// 	return $this->error($validate->errors(),"Validasi gagal!",422);
		// }

		// /* Success Validate */
		// $data = $validate->validate();
		// $user_auth = Auth::setTTL(time() + 60 * 360 * 24 * 30)->attempt($request->only('username', 'password'));
		// if (!$user_auth) {
  //           return $this->error([], 'Username / password salah',401);
  //       }
  //       $user = User::where('username', $request->username)->first();
        
  //       return $this->success(['user' => $user, 'token' => $user_auth], "Successfully login");
	}

	public function register(Request $request)
	{
		// $validate = Validator::make($request->all(), [
  //           'username'       => 'required|alpha_num|min:4|max:10|unique:tbl_users',
  //           'email'       	 => 'required|min:4|max:30|unique:tbl_users',
  //           'password'       => 'required',
  //       ], messageForValidator(), aliasRequestNameInValidator());

		// if ($validate->fails()) {
  //           return $this->error($validate->errors(),"Validasi gagal!",422);
  //       }

  //       $data = $validate->validate();
        
  //       $data["password"] = Hash::make($request->password);
  //       $data["role"] 	  = 1;
  //       $data["status"] 	  = 1;

  //       $user = User::create($data);
  //       return $this->success($user,"Berhasil register!");
	}
}

?>