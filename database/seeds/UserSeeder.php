<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\{User};
use Illuminate\Support\Facades\{Hash};
use Carbon\Carbon;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
	private $userData = [];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
    	$date = new Carbon();
        for ($i=0; $i < 10000; $i++) {
            $userData[] = [
                'username' => $faker->userName,
                'email' => $faker->freeEmail,
                'password' => Hash::make('iqbal'),
                'role' => 1,
                'status' => 1,
                'created_at' => $date->toDateTimeString(),
                'updated_at' => $date->toDateTimeString(),
            ];
        }


        $chunks = array_chunk($userData, 5000);

        foreach ($chunks as $chunk) {
            User::insert($chunk);
        }
    }
}
